import sys

import VectorModels as vm
import parser_apt as parser

from threshold import *

# data needed for ranking
document_list = parser.allArticleNamesFromFolder( DOCUMENTS_DIR )
all_queries = parser.allQueryNamesFromFolder( QUERY_DIR )
stop_words = parser.getStopWords( STOP_WORDS_FILE )
all_words = vm.loadAllWords( ALL_WORDS_FILE )
all_vectors = vm.Vectors( DATA_DIR + VECTOR_FILE )

"""
Retrieves the pickled similarity matrix file and formats it
to the adjacency matrix format.
"""
def generate_adjacency_matrix(similarity_matrix_file = SIMILARITY_MATRIX_FILE):
	sim_mat = vm.SimilarityMatrix(similarity_matrix_file)

	N = len(sim_mat.getSimilarityMatrix())

	mat = []
	for i1 in range(N):
		mat.append([])
		for i2 in range(N):
			similarity = sim_mat.getSimilarityValue(i1, i2)

			# dcouments are not connected to itself
			if abs(similarity - 1.0) < 10e-9:
				mat[i1].append(0.0)
				continue

			# if the similarity is above the threshold
			# then construct a link between documents
			if similarity >= THRESHOLD:
				mat[i1].append(1.0)
			else:
				mat[i1].append(0.0)

	return mat