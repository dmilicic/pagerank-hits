"""This module includes parser_apt.py module"""
import cPickle
from collections import Counter

import parser_apt
import numpy
from numpy import linalg as lin
import h5py

def bagOfWordsCreator(wordList):
    """ TO DO... Will only be used for tf-idf """
    return Counter(wordList)

class Vectors():
    def __init__(self, path):
        """ Initializes Vectors class

        Parameters
        ----------
        path : string
               path to file for reading created with h5py module
        """
        vectorsFile = loadAllVectors(path)
        self.matrix = vectorsFile["vectors"]

    def getMatrix(self):
        """ Returns matrix of document vectors"""
        return self.matrix

    def getVector(self, index):
        """ Returns vector of a document

        Parameters
        ----------
        index : int
                Number of desired document in matrix
        """
        return self.matrix[index]

    def getSize(self):
        """ Returns the number of document vectors

        Returns
        -------
        numberOfVectors : int
        """
        return self.matrix.shape[0]

def loadAllWords(path):
    """ Loads all words from pickle file

    Parameters
    ----------
    path : file path to pickle type file

    Returns
    -------
    List of all words
    """
    return cPickle.load(open(path, "rb"))

def loadAllVectors(path):
    """ Load all article vectors from h5py file

    Parameters
    ----------
    path : string
           path to file for reading created with h5py module
    Returns
    -------
    vectorsFile : h5py._hl.files.File
    """
    return h5py.File(path, "r")

class SimilarityMatrix():
    def __init__(self, path):
        """ Class for using similarity matrix

        Parameters
        ----------
        path : string
               Path to pickle file
        NOTE: pickle file has .p ending, file name should be similarityMatrix.p
        """
        self.similarityMatrix = cPickle.load(open(path, "rb"))

    def getSimilarityValue(self, index1, index2):
        """ Returns similarity value between two documents represented
            by index1 and index2

        Parameters
        ----------
        index1 : int
                 Row document index
        index2 : int
                 Column document index

        Returns
        -------
        float similarity value between two documents
        """
        return self.similarityMatrix[index1][index2]

    def getSimilarityMatrix(self):
        """ Returns whole similarity matrix

        Returns
        -------
        list of lists, document similarity matrix
        """
        return self.similarityMatrix

class Binary():
    def __init__(self, allWordsList):
        """ Initialise binary vector model

        Parameteres
        -----------
        allWordsList : List of strings
                       Sorted list of words gathered from all articles
        """
        self.allWordsList = sorted(allWordsList)
        self.process = parser_apt.ProcessString()
        self.stopWords = None

    def setStopWords(self, stopWordsSet):
        """ Sets the stopWords variable

        Parameters
        ----------
        stopWordsSet : Set of string values
                       Strings are words gathered from all articles
        """
        self.stopWords = stopWordsSet

    def createVectorFromPath(self, path):
        """ Creates binary vector model from file path

        Parameters
        ----------
        path : String
               Path to the text file
        """
        if (self.stopWords is None):
            raise Exception("stopWords parameter is not set")
        text = ""
        with open(path, "r") as f:
            text = f.read()
        wordSet = self.process.extractWords(text, self.stopWords)[1]
        return self.createVector(wordSet)
        
    
    def createVector(self, fileWordsSet):
        """Creates a binary vector using two sets of words

        Parameters
        ----------
        fileWordsSet : Set of strings
                       First argument. Set of words present in
                       corresponding article.
        allWordsList  : list of strings
                       Second argument. List of words formed by union of words
                       from all observed articles. All words in list are unique.
                       NOTE: list should be sorted
        Returns
        -------
        numpy.array binary vector with length same as allWordsList where the value
        of array[i] is 1.0 if the word from allWordsList[i] is present in
        fileWordsSet, 0.0 otherwise
        
        """
        fileNameWordList = []
        appendValue = fileNameWordList.append
        for word in self.allWordsList:
            if word in fileWordsSet:
                appendValue(1.0)
            else:
                appendValue(0.0)
        return numpy.array(fileNameWordList)

    def VectorModelFromWords(self, bagsOfWordsList,
                      destinationFolder):
        """Creates binary vectors for all articles in bagsOfWordsList

        Parameters
        ----------
        bagsOfWordsList   : list of sets with strings as elements
                            First argument. Sets in list are representing
                            articles. Their order in list is defined by
                            article names.
        allWordsSet       : set of strings
                            Second argument. All different words collected
                            from all articles. Article texts are processed by
                            parser_apt.ProcessString class.
                            NOTE: look parser_apt.ProcessString.extractWords
        destinationFolder : string
                            Third argument. Path to folder where data
                            will be stored.

        Function stores all created vectors to a destinationFolder + "vectors"
        file as numpy.array of numpy.arrays.
        NOTE: look createBinVector
        """
        
        storageFile = h5py.File(destinationFolder + "vectors", "w")

        matrix = []
        appendFunc = matrix.append
        
        for wordsSet in bagsOfWordsList:        
            appendFunc(self.createVector(wordsSet))

        storageFile.create_dataset("vectors", data = numpy.array(matrix))
        storageFile.close()

def storeAllWords(allWordsList, destinationFolder):
    """ Function stores allWordsSet to a destinationFolder + "all_words.p"
    in pickle file via cPickle module in form of a sorted list. 

    Parameteres
    -----------
    allWordsList      : Sorted list of strings
                        First argument. All different words collected
                        from all articles. Article texts are processed by
                        parser_apt.ProcessString class. List is sorted
                        NOTE: look parser_apt.ProcessString.extractWords
    destinationFolder : string
                        Second argument. Path to folder where data
                        will be stored.
    """
    cPickle.dump(allWordsList, open( destinationFolder + "all_words.p", "wb"))

def buildSimilarityMatrix(vectorMatrix, destinationFolder):
    """ Creates similarity matrix between documents

    Parameters
    ----------
    vectorMatrix      : h5py._hl.dataset.Dataset
                        Collection of all document vectors

    destinationFolder : string
                        Path for folders destination
    
    """
    length = vectorMatrix.shape[0]
    rows = [[1 for i in range(length)] for j in range(length)]

    for vectorIndex1 in range(length - 1):
        for vectorIndex2 in range(vectorIndex1, length):
            cosine = cosineSimilarity(vectorMatrix[vectorIndex1],
                                      vectorMatrix[vectorIndex2])
            rows[vectorIndex1][vectorIndex2] = cosine
            rows[vectorIndex2][vectorIndex1] = cosine

    cPickle.dump(rows, open( destinationFolder +
                             "similarityMatrix.p", "wb"))
            
def cosineSimilarity(arrayOne, arrayTwo):
    """Calculates cosine similarity between two numpy.arrays

    Parameters
    ----------
    arrayOne    : numpy.array of numpy.float64-s
               First argument.
    arrayTwo : numpy.array of numpy.float64-s
               Second argument.
    NOTE: parameters length must be the same

    Returns
    -------
    numpy.float64 value
    """
    a = lin.norm(arrayOne) * lin.norm(arrayTwo)
    if a == 0:
        raise Exception("Zero devision")
    return numpy.dot(arrayOne, arrayTwo) / a

