import os.path

import numpy as np
import cPickle as pickle

from utility import *
from performance import *

class PageRank():

	# filename constants
	STORE_FILE = "pagerank_scores.p"

	def __init__(self, adj_mat = None, pagerank_score_file = None):

		# there is no already generated file with pagerank scores
		if(pagerank_score_file == None):
			if adj_mat == None:
				raise Exception("Adjacency matrix can not be None!")
			self.initPageRank(adj_mat)	# start the pagerank algorithm
			return

		if not os.path.isfile(pagerank_score_file):
			raise IOException("File not found!")

		# load the pagerank scores from a file
		self.load_pagerank_scores(pagerank_score_file)

	def initPageRank(self, adj_mat):
		# get the matrix length
		self.N = len(adj_mat)

		# adjust adjacency matrix for probability
		self.probability_adjustment(adj_mat)

		# convert the matrix to numpy format
		self.H = np.matrix(adj_mat)

		# vector of ones
		self.e = np.matrix(np.ones(self.N)).T

		# vector of sink link pages - pages with no outgoing links have 1
		# at their corresponding index in this vector, 0 otherwise
		self.a = np.matrix([self.sink_pages]).T

		# normalized matrix of ones
		self.E = 1.0/self.N * self.e * self.e.T

		# random surfer stochasticity adjustment
		self.S = self.H + self.a * (1.0/self.N * self.e.T)

		# probability that a random surfer will teleport to another page
		self.ALFA = 0.8

		# initial pagerank scores
		self.I = np.matrix(np.ones(self.N))

		# start the pagerank algorithm until convergence
		self.iterate()

	"""
	Given a query returns all documents that are considered similar by cosine similarity.
	Documents are sorted based on their pagerank score.
	"""
	def search(self, query_file):
		vector_model = vm.Binary(all_words)
		vector_model.setStopWords(stop_words)

		# create a vector model of words from the query
		query_vector = vector_model.createVectorFromPath(query_file)

		# resulting documents will be put here
		result = []

		# self.data is a list of tuples in the form (document_name, score)
		for item in self.data:
			doc_name = item[0]
			pr_score = item[1]

			# get the vector model for the current document
			idx = document_list.index(doc_name)
			doc_vector = all_vectors.getVector(idx)

			# calculate the similarity between the query and the document
			similarity = vm.cosineSimilarity(query_vector, doc_vector)

			# print doc_name, pr_score

			# if the similarity is above a threshold, add the document to result list
			if similarity >= RELEVANCE_THRESHOLD:
				# print doc_name, pr_score, similarity
				result.append(doc_name)

		# print "Resulting documents #: ", len(result)

		return result

	"""
	Resulting scores of the pagerank algorithm are returned as a numpy matrix.
	This method converts that matrix to a list of tuples in the format (document_name, pagerank_score).
	This list is sorted so the document with the highest score comes first.
	"""
	def formalize_data(self):
		# format the numpy matrix of scores to a normal list
		scores = self.I.tolist()[0]

		# create a list of the format (doc_name, score) so we can see
		# the name and the score of each document
		scores_with_docname = []
		for idx in range(len(scores)):
			scores_with_docname.append((document_list[idx], scores[idx]))

		# sort it so the document with the highest score comes first
		scores_with_docname.sort(key = lambda tup: tup[1], reverse=True)

		# make this converted data a member variable
		self.data = scores_with_docname

		return scores_with_docname

	def store_pagerank_scores(self, target_file = STORE_FILE):
		data = self.formalize_data()
		pickle.dump(data, open( DATA_DIR + target_file, "wb"))

	def load_pagerank_scores(self, scores_file = STORE_FILE):
		data = pickle.load(open( DATA_DIR + PageRank.STORE_FILE, "rb"))
		self.data = data
		return data

	"""
	Iterates through the PageRank algorithm steps until convergence.
	"""
	def iterate(self):
		# calculate iterations until convergence
		while True:
			current_iteration = self.calcIteration(self.I)
			if (self.I == current_iteration).all():
				break
			self.I = current_iteration

		self.store_pagerank_scores()

	"""
 	Calculates a single iteration of the PageRank algorithm.
	"""
	def calcIteration(self, I):
		return self.ALFA * I * self.H + (self.ALFA * self.I * self.a + 1 - self.ALFA) * self.e.T / self.N

	"""
	Adjusts the adjacency matrix so that it contains a probability distribution
	for visiting other pages/documents. For example, if a document is connected
	only to pages 2 and 3 then the adjacency matrix will show [0, 1, 1, 0, 0, ...],
	however, when adjusted for probability the adjacency matrix will show
	[0, 0.5, 0.5, ...]. In other words, all ones are divided by the number of pages
	the current page has a link to.
	"""
	def probability_adjustment(self, adj_mat):
		N = self.N

		# put 1 on a document that has no outgoing links - sink page,
		# otherwise put 0
		self.sink_pages = []

		# adjust for probability
		for i1 in range(N):
			# count outgoing links for this document
			link_cnt = 0
			for i2 in range(N):
				if adj_mat[i1][i2] == 1.0:
					link_cnt += 1

			# document has NO outgoing links
			if link_cnt == 0:
				self.sink_pages.append(1.0)
				continue
			else:	# document has outgoing links
				self.sink_pages.append(0.0)

			# adjust the adjacency matrix for probability
			for i2 in range(N):
				adj_mat[i1][i2] /= link_cnt

if __name__ == "__main__":
	adj_mat = generate_adjacency_matrix()

	pr = PageRank(adj_mat)
	# pr = PageRank(pagerank_score_file = DEFAULT_PAGERANK_FILE)

	query = sys.argv[1]

	result = pr.search(QUERY_DIR + query)

	for r in result[:10]:
		print r

	# query = query[:-4]
	# p = Performance()
	# print p.precision(query, result)
	# print p.recall(query, result)
	# print p.Fscore(query, result)


