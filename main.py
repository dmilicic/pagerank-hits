from pagerank import *
from hits import *
from binaryAlg import *

	########################################################
	############## BINARY VECTOR ALGORITHM #################
	########################################################

def runBinaryVector(n, performance):
	print
	print "Running binary vector algorithm..."
	print

	bin_alg = BinAlgorithm()

	R_precisions = []
	avg_precisions = []

	total_precision = 0.0
	total_recall = 0.0
	total_fscore = 0.0

	for query in all_queries:
		result = bin_alg.search(QUERY_DIR + query)

		# remove the .txt suffix
		query = query[:-4]

		precision = performance.precision(query, result)
		recall = performance.recall(query, result)
		fscore = performance.Fscore(query, result)

		print query, " Precision: ", precision, " Recall: ", recall, " F-score: ", fscore

		total_precision += precision
		total_recall += recall
		total_fscore += fscore

		R_precisions.append(performance.R_precision(query, result))
		avg_precisions.append(performance.AverPrecision(query, result))

	print
	print "Binary vector algorithm results"
	print "-----------------------------"
	print "Avg. precision: ", total_precision / n
	print "Avg. recall: ", total_recall / n
	print "Avg. F-score: ", total_fscore / n

	# display r-precision and MAP metrics
	print "R-Precision: ", sum(R_precisions) / n
	print "MAP: ", sum(avg_precisions) / n



	########################################################
	############## PAGERANK ALGORITHM ######################
	########################################################

def runPageRank(n, adj_mat, performance):
	print
	print "Running PageRank algorithm..."
	print

	pr = PageRank(adj_mat)

	R_precisions = []
	avg_precisions = []

	total_precision = 0.0
	total_recall = 0.0
	total_fscore = 0.0
	for query in all_queries:

		# remove the .txt suffix
		query = query[:-4]

		# there is no data for this query
		if query not in performance.relevant_docs.keys():
			continue

		result = pr.search(QUERY_DIR + query + ".txt")
		precision = performance.precision(query, result)
		recall = performance.recall(query, result)
		fscore = performance.Fscore(query, result)

		print query, " Precision: ", precision, " Recall: ", recall, " F-score: ", fscore

		R_precisions.append(performance.R_precision(query, result))
		avg_precisions.append(performance.AverPrecision(query, result))

		total_precision += precision
		total_recall += recall
		total_fscore += fscore

	print
	print "PageRank algorithm results"
	print "-----------------------------"
	print "Avg. precision: ", total_precision / n
	print "Avg. recall: ", total_recall / n
	print "Avg. F-score: ", total_fscore / n

	# display r-precision and MAP metrics
	print "R-Precision: ", sum(R_precisions) / n
	print "MAP: ", sum(avg_precisions) / n


	########################################################
	############## HITS ALGORITHM ##########################
	########################################################


def runHits(n, adj_mat, performance):
	print
	print "Running HITS algorithm..."
	print

	R_precisions = []
	avg_precisions = []

	adj_mat = generate_adjacency_matrix()

	total_precision = 0.0
	total_recall = 0.0
	total_fscore = 0.0
	for query in all_queries:

		# remove the .txt suffix
		query = query[:-4]

		# there is no data for this query
		if query not in performance.relevant_docs.keys():
			continue

		hits = HITS(QUERY_DIR + query + ".txt", adj_mat)
		result = hits.search_results()

		precision = performance.precision(query, result)
		recall = performance.recall(query, result)
		fscore = performance.Fscore(query, result)

		print query, " Precision: ", precision, " Recall: ", recall, " F-score: ", fscore

		R_precisions.append(performance.R_precision(query, result))
		avg_precisions.append(performance.AverPrecision(query, result))

		total_precision += precision
		total_recall += recall
		total_fscore += fscore

	print
	print "HITS algorithm results"
	print "----------------------"
	print "Avg. precision: ", total_precision / n
	print "Avg. recall: ", total_recall / n
	print "Avg. F-score: ", total_fscore / n

	# display r-precision and MAP metrics
	print "R-Precision: ", sum(R_precisions) / n
	print "MAP: ", sum(avg_precisions) / n


if __name__ == "__main__":
	print
	print "=========================================================="
	print "************* PageRank/HITS TASK ASSIGNMENT **************"
	print "=========================================================="
	print

	print "Query #: ", len(all_queries), " Article #: ", len(document_list), " Similarity threshold: ", THRESHOLD
	print "------------------------------------------------------------"

	# get the number of queries
	n = len(all_queries)

	print
	print "Generating adjacency matrix..."
	print
	adj_mat = generate_adjacency_matrix()

	# init the performance class
	performance = Performance()

	########################################################
	############## BINARY VECTOR ALGORITHM #################
	########################################################

	runBinaryVector(n, performance)

	########################################################
	############## PAGERANK ALGORITHM ######################
	########################################################

	runPageRank(n, adj_mat, performance)

	########################################################
	############## HITS ALGORITHM ##########################
	########################################################

	runHits(n, adj_mat, performance)