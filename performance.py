
from threshold import RELEVANCE_JUDGEMENT

class Performance():

	def __init__(self, filename = RELEVANCE_JUDGEMENT):

		relevant_docs = dict()

		fin = open(filename, "r")
		content = fin.readlines()

		# extract data to create a dictionary of relevant documents for each query
		for line in content:

			# skip empty lines
			if line == '\n' or line == "\r\n":
				continue

			splitted = line.split(" ")
			query = splitted[0].lower()
			doc = splitted[1]

			# dictionary is in the form of query -> doc1, doc2, ...
			try:
				relevant_docs[query].append(doc)
			except:
				relevant_docs[query] = []
				relevant_docs[query].append(doc)

		self.relevant_docs = relevant_docs


		# for x in relevant_docs:
		# 	print x, relevant_docs[x]

	def precision(self, query, result):

		tp = 0.0	# true positives
		fp = 0.0	# false positives

		for doc in result:
			if doc in self.relevant_docs[query]:
				tp += 1.0
			else:
				fp += 1.0

		# print tp, fp

		# we can't divide by zero
		if tp + fp == 0:
			return 0.0

		return tp / (tp + fp)

	def recall(self, query, result):

		tp = 0.0	# true positives
		fn = 0.0	# false negatives

		for doc in result:
			if doc in self.relevant_docs[query]:
				tp += 1.0

		for doc in self.relevant_docs[query]:
			if doc not in result:
				fn += 1

		# we can't divide by zero
		if tp + fn == 0:
			return 0.0

		return tp / (tp + fn)

	def Fscore(self, query, result):
		precision = self.precision(query, result)
		recall = self.recall(query, result)

		# we can't divide by zero
		if (precision + recall) == 0.0:
			return 0.0

		return 2 * precision * recall / (precision + recall)

	def R_precision(self, query, result):
		tp = 0.0	# true positives

		# number of relevant documents for query
		R = len(self.relevant_docs[query])

		for doc in result[:R]:
			if doc in self.relevant_docs[query]:
				tp += 1.0
		return tp / (R)

	def AverPrecision(self, query, result):
		tp = 0.0	# true positives
		fp = 0.0	# false positives

		R = len(self.relevant_docs[query])

		precisions = []

		for doc in result:
			if doc in self.relevant_docs[query]:
				tp += 1.0
				precisions.append(tp / (tp+fp))
			else:
				fp += 1.0

		# print tp, fp

		# we can't divide by zero
		if tp + fp == 0:
			return 0.0

		return sum(precisions) / R


if __name__ == "__main__":
	p = Performance()

	print p.precision("query40", ["article-7893.xml", "article-204.xml", "article-9193.xml"])
	print p.recall("query40", ["article-7893.xml", "article-204.xml", "article-9193.xml"])
	print p.Fscore("query40", ["article-7893.xml", "article-204.xml", "article-9193.xml"])
