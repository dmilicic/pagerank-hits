
from utility import *
from performance import *

class HITS():

	def __init__(self, query_file, adj_mat):
		self.N = len(adj_mat)
		self.adj_mat = adj_mat

		# print "Total set length: ", len(adj_mat)

		# generate the base set
		self.generate_base_set(query_file, adj_mat)

		# print "Base set length: ", len(self.base_set)

		self.N = len(self.base_set)

		# set the hub and authority score of all pages to the initial value of 1
		self.auth = [1.0] * self.N
		self.hub  = [1.0] * self.N

		# start the algorithm
		self.calcScores()

	def search_results(self, relevance_threshold = RELEVANCE_THRESHOLD):
		result = []

		# self.data is in the form of (doc_name, authority_score)
		for item in self.data:
			doc_name = item[0]
			auth = item[1]

			# print doc_name, auth

			# document is relevant
			if self.similarities[doc_name] >= relevance_threshold:
				result.append(doc_name)
				# print doc_name, auth, self.similarities[doc_name]

		return result

	"""
	This method converts the list of authority scores to a list of
	tuples in the format (document_name, authority_score).
	This list is sorted so the document with the highest score comes first.
	"""
	def formalize_data(self):

		# create a list of the format (doc_name, score) so we can see
		# the name and the score of each document
		scores_with_docname = []
		for idx in range(len(self.auth)):
			scores_with_docname.append((self.base_set[idx], self.auth[idx]))

		# sort it so the document with the highest score comes first
		scores_with_docname.sort(key = lambda tup: tup[1], reverse=True)

		# make this converted data a member variable
		self.data = scores_with_docname

		return scores_with_docname

	def calcScores(self, steps = 10):

		for step in range(steps):

			# init normalization for authority score
			norm = 0

			# update all authority values
			for doc in self.base_set:
				idx = self.base_set.index(doc)
				self.auth[idx] = 0.0

				# authority score equals the sum of all hub scores
				# of pages that point to the current page
				for i in range(self.N):
					if self.adj_mat[idx][i] == 1.0:
						self.auth[idx] += self.hub[i]

				# add the square of authority for normalization
				norm += self.auth[idx] ** 2

			norm = norm ** (0.5)

			# normalize authority scores
			for doc in self.base_set:
				idx = self.base_set.index(doc)
				self.auth[idx] /= norm

			# init normalization for hub score
			norm = 0

			# update all hub scores
			for doc in self.base_set:
				idx = self.base_set.index(doc)
				self.hub[idx] = 0.0

				# hub score equals the sum of all authority scores
				# of pages that the current page points to
				for i in range(self.N):
					if self.adj_mat[idx][i] == 1.0:
						self.hub[idx] += self.auth[i]

				# add the square of hub score for normalization
				norm += self.hub[idx] ** 2

			norm = norm ** (0.5)

			# normalize hub score
			for doc in self.base_set:
				idx = self.base_set.index(doc)
				self.hub[idx] /= norm

		# convert the result to the desired format
		self.formalize_data()

	def generate_base_set(self, query_file, adj_mat):
		vector_model = vm.Binary(all_words)
		vector_model.setStopWords(stop_words)

		# create a vector model of words from the query
		query_vector = vector_model.createVectorFromPath(query_file)

		# resulting documents will be put here
		result = []

		# stores the similarities of documents to this query
		self.similarities = dict()

		# dictionary that stores which documents belong to the root set
		docs_taken = dict()

		# get the most similar documents in the root set
		for doc in document_list:

			# get the vector model for the current document
			idx = document_list.index(doc)
			doc_vector = all_vectors.getVector(idx)

			# calculate the similarity between the query and the document
			similarity = vm.cosineSimilarity(query_vector, doc_vector)

			# store the similarity
			self.similarities[doc] = similarity

			# if the similarity is above a threshold, add the document to result list
			if similarity >= RELEVANCE_THRESHOLD:
				result.append(doc)
				docs_taken[doc] = 1

		# add documents that are linked to from the root set, thus creating the base set
		for idx in range(self.N):
			for val in range(self.N):
				if adj_mat[idx][val] == 1.0 and document_list[idx] not in result:
					result.append(document_list[idx])

		self.base_set = result

		return result


if __name__ == "__main__":
	adj_mat = generate_adjacency_matrix()

	query = sys.argv[1]

	hits = HITS(QUERY_DIR + query, adj_mat)
	result = hits.search_results()

	for r in result[:10]:
		print r

	# p = Performance()
	# print p.precision(query, result)
	# print p.recall(query, result)
	# print p.Fscore(query, result)


