import sys
from xml.etree.ElementTree import parse as xmlParse

import VectorModels
import parser_apt

BINARY = True # binary vector space model

if __name__ == '__main__':
    # Input system arguments via command line or manually below.
    # First: path to folder conaining all documents
    # Second: file path to stopwords file
    # Third path to destination folder for data storage

    sys.argv = [sys.argv[0], './Documents/', './stop_words.txt', './Data/']

    if len(sys.argv) != 4:
        raise Exception("Invalid number of arguments!")

    folderPath = sys.argv[1]
    stopWordsPath = sys.argv[2]
    destinationFolderPath = sys.argv[3]

    fileList = parser_apt.allArticleNamesFromFolder(folderPath)

    stopWordsSet = parser_apt.getStopWords(stopWordsPath)

    bagOfWords = []
    allWords = set()
    process = parser_apt.ProcessString()


    for fileName in fileList:
        xmlTree = xmlParse(folderPath + fileName)
        # xmlTree.find("text") method finds tag "text" in xml hierarchy
        wordList, wordSet  = \
                process.extractWords(xmlTree.find("text").text.encode("utf8"),
                                     stopWordsSet)
        if BINARY:
            bagOfWords.append(wordSet)
        allWords.update(wordSet)

    vectorModel = VectorModels.Binary(allWords)
    vectorModel.VectorModelFromWords(bagOfWords, destinationFolderPath)
    VectorModels.storeAllWords(vectorModel.allWordsList, destinationFolderPath)

    # creating similarity matrix
    vectorsFile = VectorModels.loadAllVectors(destinationFolderPath+"vectors")
    articleVectorsMatrix = vectorsFile["vectors"]
    VectorModels.buildSimilarityMatrix(articleVectorsMatrix,
                                       destinationFolderPath)
    vectorsFile.close()
