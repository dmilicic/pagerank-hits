Instructions:

1. build the vector data: python build_vector_model.py

1.5. set the thresholds in threshold.py

2. run a specific algorithm, ex: python pagerank.py query1.txt, python hits.py query2.txt, python binaryAlg.py query3.txt

3. run all algorithms against all queries with detailed analysis: python main.py
