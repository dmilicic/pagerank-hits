
# similarity threshold for cosine similarity
THRESHOLD = 0.15

# threshold for relevant documents
RELEVANCE_THRESHOLD = THRESHOLD

# file and folder constants
DOCUMENTS_DIR = "./Documents/"
DATA_DIR = "./Data/"
QUERY_DIR = "./Queries/"
STOP_WORDS_FILE = "./stop_words.txt"
VECTOR_FILE = "vectors"
ALL_WORDS_FILE = "./Data/all_words.p"
SIMILARITY_MATRIX_FILE = "./Data/similarityMatrix.p"
DEFAULT_PAGERANK_FILE = "./Data/pagerank_scores.p"
RELEVANCE_JUDGEMENT = "relevance-judgements.txt"