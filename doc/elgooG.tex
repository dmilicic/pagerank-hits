% Tenplate for TAR 2014
% (C) 2014 Jan Šnajder, Goran Glavaš
% KTLab, FER

\documentclass[10pt, a4paper]{article}

\usepackage{tar2014}

\usepackage[utf8]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{hyperref}

\title{Link analysis algorithms (HITS and PageRank) in the
information retrieval context}

%VAŽNO: Zakomentirajte sljedeću liniju kada šaljete rad na recenziju
\name{Damir Ciganović-Janković, Teon Banek, Dario Miličić}

\address{
University of Zagreb, Faculty of Electrical Engineering and Computing\\
Unska 3, 10000 Zagreb, Croatia\\
\texttt{damir.ciganovic-jankovic@fer.hr}, \texttt{teon.banek@fer.hr}, \texttt{dario.milicic@gmail.com}\\
}


\abstract{
The research is done within the Text analysis and retrieval course as a project assignment.  We implemented a information retrieval (IR) system in Python using a binary vector-space retrieval model and evaluated it along with both, PageRank and HITS algorithms on the set of queries with labelled relevant documents. We used European Media Monitor (EMM) test collection as the evaluation dataset. For link analysis algorithms to work, we constructed a bidirectional link between documents that were determined to be similar using cosine similarity function. Performance was measured using standard IR Metrics, R-Precision and Mean Average Precision. After evaluation of all three algorithms we draw a conclusion that link analysis algorithms do not improve information retrieval models for this data set.
}

\begin{document}

\maketitleabstract

\section{Introduction}

Surfing through the internet has never been easier. Only a few clicks and several keyboard
keys pressed and we are already getting what we want from our favourite web browser. We could easily
say that browsers these days work so good that it seems like they know what we know even before we
type it. To realize how is it possible to produce such good results, we must look into algorithms
being used for retrieving relevant web pages for a given query. These algorithms are called link
analysis algorithms.  The aim of this project is to build an information retrieval system that ranks
the documents based on the analysis of between-document links using PageRank and HITS link analysis
algorithms [5]. Primary hypothesis is that with the help of PageRank and HITS algorithms, system should get
better results in retrieving relevant documents for a given query than using just the binary vector
space model. Our goal is to test that thesis and to check which one of the two algorithms will score
better results under the same conditions. This research will help us to better understand how web
searching is done.

\section{Dataset description}

For testing and evaluation we used European Media Monitor (EMM) IR test
collection [7]. It contains documents, queries and relevance judgements for each
of the queries. Every document contains text that is a news report from some news report company.
Documents are stored as .xml files. Queries are represented as .txt file containing one or more
sentences. Relevance judgements for all queries are given in a single .txt file. Every line in the
file is a triplet separated by spaces. Triplets are consisting of: query name, article name and
relevance label. There is in total 1741 documents and 50 queries, but there is only 48 queries with
their relevance judgements attached.

\section{Information retrieval metrics}

For evaluation of ranked retrieval results there are several different metrics. In this paper, primary metrics were R-precision and Mean Average Precision [6].

R-precision metric is calculated as follows. For given set of known relevant documents \textit{Rel} for query \textit{q} we produce ranked results by one of the algorithms. Then we take first \textit{R} values \textit{bestR} from produced results where \textit{R} is the number of relevant documents for query \textit{q}. For every document in \textit{bestR} we check if it is one of the relevant documents and if it is, we add it to the \textit{r}. R-precision value is $r/R$. R-precision calculates precision for one point only so to get the whole model evaluation, we calculated arithmetic mean of R-precision values for every query.

Mean Average Precision is an arithmetic mean of average precisions for every query. Average precision is calculated as follows. If the set of relevant documents for a query $q \in Q$ is $\{ d_1, d_2,\ldots, d_m\}$ and $R_k$ is the set of ranked retrieval results from the top result until you get to the document $d_k$, then average precision \textit{AP} is: 

\begin{equation}\label{eq:average precision}
AP(q) = \frac{1}{m}\sum_{k=1}^m
Precision(R_k)
\end{equation}


\section{Binary vector space model [1]}
\label{sec:section-BVM}

Building information retrieval system requires a few steps, including some natural language processing methods. For a given text, we
extracted all words by splitting text on every white-space and striping all punctuation marks. After
that, the words are stemmed using the Porter Stemmer to narrow down the difference between words
with same root [2]. Before the next step we must take into account
that in the text articles, there is always a set of words that is common for every text regardless
of topic discussed. That set is called stop-words set and it is also removed from the set of article
words. We compiled that set of words by combining two sets [3][4]. Total number of stop-words is 571.

What we have by now is all different words from a document. After we did that for every document, we compiled a set
of all words from all articles. Now, to build a binary vector from the article, we search for every
word from all-words set and if it is found in an article, we set the value for that word to 1, and 0
otherwise. This is what makes it a binary model. Total vector length is therefore the same as the number of words in all-words set, which is 21057.

\section{Determining whether a document is relevant for a given query}

To determine if a document is relevant for a given query we first converted the query text into a vector in the same way as we converted documents into vectors. Query vector size is the same as the document vector size because we use the same all-words set. After that, we determine for every document if it is relevant for a given query or not, and then we sort all relevant documents in specific order based on the algorithm used.

For each algorithm, we have measured performance using mentioned IR metrics. All calculations were done in Python 2.7 on a Mac OS X, version 10.9.3 using 1.3 Ghz Intel Core i5.

\subsection{Binary vector space model without link analysis algorithms}

For binary vector space model alone, we used cosine similarity between query vector and every document vector [10]. If the cosine similarity is above a certain threshold, we label that document as relevant for the query, and not relevant otherwise. After that step, we rank our relevant documents based on cosine similarity value, from highest to lowest to get ranked results.


\begin{table}
\caption{\textit{R-precision} and \textit{Mean Average Precision} values for given threshold, binary vector model}
\label{tab:BVM}
\begin{center}
\begin{tabular}{lcr}
\toprule
Sim. threshold & R-precision & MAP\\
\midrule
0.05 & 0.2247 & 0.2397 \\
0.1 & 0.2247 & 0.2397 \\
0.15 & 0.2179 & 0.2094 \\
0.2 & 0.1484 & 0.1306 \\
0.25 & 0.0567 & 0.0540 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}


We set the similarity threshold to 0.1. It should be noted that similarity and relevance threshold are the same value. To determine that value of the threshold, we evaluated the system with thresholds in range of 0.05 to 0.29 (with 0.01 step), and chose the most appropriate one. The reason why we chose 0.1 for the threshold value is because the R-precision and MAP measures are significantly dropping when threshold value is getting higher from 0.1, but not significantly rising when threshold is getting lower. Results can be seen in Table~\ref{tab:BVM}.

\subsection{Binary vector space model with PageRank}

PageRank is one of the most widely known link analysis algorithms. It was developed by Larry Page and Sergey Brin as part of a research project about a new kind of search engine [8]. It is also used commercially at Google Inc. In short, PageRank works by counting the number and quality of links to a page to determine a rough estimate of how important the website is. The underlying assumption is that more important websites are likely to receive more links from other websites.

For PageRank and HITS algorithms to work, we needed to determine links between documents in our data set. Link analysis algorithms work on a graph consisting of documents as vertices and edges as links between those documents. To convert our data set to such a graph, we constructed links based on document similarity. Similarity score was calculated for each document pair using cosine similarity with documents represented as binary word vectors. If the similarity score was above a certain threshold then we considered those documents linked with a bidirectional link. Resulting graph was represented in memory as an adjacency matrix. In the adjacency matrix \textit{M}, if the documents \textit{i} and \textit{j} are linked then \textit{M[i][j]} and \textit{M[j][i]} are equal to 1, otherwise it is equal to 0. This adjacency matrix is used for both PageRank and HITS algorithms.

PageRank is an algorithm that doesn't depend on the query as input. It is run during preprocessing stage, before a query is given to the system for evaluation. After the algorithm has finished, every document will have a PageRank score that will determine its ranking during query evaluation. The higher the score, the more important the document is.

When a query is given, it is first converted to a vector as described in Section~\ref{sec:section-BVM} This query vector is then compared to all documents from highest to lowest PageRank score. If the query vector and the document vector are similar, according to cosine similarity and the relevancy threshold, then the document is declared relevant. This way the more important documents are higher in the result list. The key difference between this approach and the approach without link analysis algorithms is that the similarity score has no impact on the ranking of documents. Similarity score is simply needed to determine if the document is relevant or not.

This makes a certain scenario possible that may seem as an error in the information retrieval system. The reasoning behind this approach should explain why is such behaviour wanted. Suppose that a similarity score of a query vector and a document is very high, for example similarity score $\alpha >= 0.99$. We might believe that this document should be at the top of the result list but with PageRank this doesn't have to be the case. This is actually a very common scenario when searching the web with Google. Consider searching for \textit{"read it"} on Google's search engine. The result will actually be the social networking website \textit{reddit} even though there is a page that is more similar to the given query. The reason for this is that reddit has a much higher PageRank score which implies that the user most probably wanted \textit{reddit} as a search result in the first place. In other words, PageRank score determines implicit probability that the user wants a certain page as a search result.

When calculating MAP and R-precision, better performance was achieved by the basic vector space model than the model which includes the PageRank algorithm. This implies that our hypothesis, which states that link analysis algorithms will improve results, may not be correct. Results can be seen in Table~\ref{tab:PageRank}.


\begin{table}
\caption{\textit{R-precision} and \textit{Mean Average Precision} values for given similarity threshold, binary vector model and PageRank.}
\label{tab:PageRank}
\begin{center}
\begin{tabular}{lcr}
\toprule
Sim. threshold & R-precision & MAP\\
\midrule
0.05 & 0.0064 & 0.0174 \\
0.1 & 0.0444 & 0.0666 \\
0.15 & 0.1612 & 0.1682 \\
0.2 & 0.1270 & 0.1093 \\
0.25 & 0.0463 & 0.0369 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\subsection{Binary vector space model with HITS}

Hyperlink-Induced Topic Search (HITS) is a link analysis algorithm developed by Jon Kleinberg [9]. Unlike PageRank, in HITS there are two scores that have to be evaluated for each document. These scores are called hub score and authority score. A page has a high hub score if it points to pages with high authority scores. Also, a page is a good authority if it is pointed by pages with high hub scores. These scores are interdependent: Hub score of a page equals the sum of the authority scores of the pages it links to; Authority score of a page equals the sum of hub scores that point to it.

In our data set, each link is bidirectional which means hub and authority scores are not going to differ. Different pages will have different scores because of different number of links but their hub and authority values will be equal given that their starting values are equal. Another difference from PageRank is that HITS takes a query as input. This will make HITS much slower than PageRank as it calculates the scores only after a query is given. However, HITS doesn't calculate hubs and authorities on the entire graph. Rather, it takes a subgraph that will be called a base set. The base set depends on the query input. Base set is generated as follows: When a query is given it is compared for similarity against all documents in the data set. All documents that have a similarity score above the threshold are added to a root set. In addition to the documents in the root set, the base set contains all the documents that are linked to from the root set. When these documents are added, the base set is ready for evaluation.

Evaluation of the binary vector model with HITS shows that it doesn't improve search results by any metric. Also, it evaluated the slowest as the entire algorithm had to be performed for each query. Results can be seen in Table~\ref{tab:HITS}.


\begin{table}
\caption{\textit{R-precision} and \textit{Mean Average Precision} values for given similarity threshold, binary vector model and HITS.}
\label{tab:HITS}
\begin{center}
\begin{tabular}{lcr}
\toprule
Sim. threshold & R-precision & MAP\\
\midrule
0.05 & 0.0060 & 0.0111 \\
0.1 & 0.0260 & 0.0406 \\
0.15 & 0.1196 & 0.1048 \\
0.2 & 0.1302 & 0.1156 \\
0.25 & 0.0567 & 0.0412 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\section{Conclusion}

It is easy to see by all metrics that both link analysis algorithms have failed to improve search results for the given data set. Another metric that wasn't measured is time but the difference was noticeable with PageRank and binary vector model being almost equally fast and HITS being the slowest.

Link analysis algorithms weren't designed for these kind of data sets but it is important to be aware that it is sometimes hard to predict without trying where certain tools can be helpful. In this research opportunity, however, our original hypothesis has not been proven.


\section*{References}

\indent

[1] Christopher D. Manning, Prabhakar Raghavan and Hinrich Shutze, Introduction to Information Retrieval, Cambridge University Press. 2008., \url{http://www-nlp.stanford.edu/IR-book/}, section 14

[2] Porter stemmer, \url{http://snowball.tartarus.org/algorithms/porter/stemmer.html}

[3] Stopwords list 1, \url{http://www.ranks.nl/stopwords}

[4] Stopwords list 2, \url{http://www.lextek.com/manuals/onix/stopwords1.html}

[5] Link analysis algorithms, Christopher D. Manning, Prabhakar Raghavan and Hinrich Shutze, Introduction to Information Retrieval, Cambridge University Press. 2008., \url{http://www-nlp.stanford.edu/IR-book/}, section 21

[6] R-precision; MAP, Christopher D. Manning, Prabhakar Raghavan and Hinrich Shutze, Introduction to Information Retrieval, Cambridge University Press. 2008., \url{http://www-nlp.stanford.edu/IR-book/} , section 8

[7] EMM-IR-Collection, \url{http://emm.newsbrief.eu/}

[8] The Anatomy of a Large-Scale Hypertextual Web Search Engine, Sergey Brin and Lawrence Page, \url{http://infolab.stanford.edu/~backrub/google.html}

[9] Hyperlink-Induced Topic Search, \url{https://en.wikipedia.org/wiki/HITS_algorithm}

[10] Cosine similarity, \url{https://en.wikipedia.org/wiki/Cosine_similarity}


\end{document}

