from  utility import *
from performance import *

BINARYALG_THRESHOLD = THRESHOLD

class BinAlgorithm():
    def __init__(self):
        self.vector_model = vm.Binary(all_words)
        self.vector_model.setStopWords(stop_words)
        self.treshold = BINARYALG_THRESHOLD

    def search(self, query_file):
        query_vector = self.vector_model.createVectorFromPath(query_file)

        scores = [(vm.cosineSimilarity(query_vector,
                                                 all_vectors.getVector(i)),
                         document_list[i])
                    for i in range(all_vectors.getSize())]

        result = []

        for document in sorted(scores, reverse = True):
            if document[0] >= self.treshold:
                result.append(document[1])
        return result

    def setTreshold(self, treshold):
        self.treshold = treshold

def evaluationBinAlg(binAlg, p):
    R_precisions = []
    AverPrecisions = []

    for query in all_queries:
        result = binAlg.search(QUERY_DIR + query)

        query_name = query.split(".")[0]

        R_precisions.append(p.R_precision(query_name, result))
        AverPrecisions.append(p.AverPrecision(query_name, result))
    return R_precisions, AverPrecisions


if __name__ == "__main__":


    query = sys.argv[1]

    bin_alg = BinAlgorithm()
    result = bin_alg.search(QUERY_DIR + query)

    for r in result[:10]:
        print r

    # P = Performance()
    # size = len(all_queries)
    # evalResult = []
    # while(BinAlg.treshold <= 0.1):
    #     print BinAlg.treshold
    #     r, aver = evaluationBinAlg(BinAlg, P)
    #     evalResult.append((BinAlg.treshold, sum(r)/size, sum(aver)/size))
    #     BinAlg.treshold += 0.01


    # x1 = sorted(evalResult, key = lambda x: x[1], reverse=True)
    # x2 = sorted(evalResult, key = lambda x: x[2], reverse=True)


