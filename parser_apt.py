import os
    
class ProcessString():
    """ Class for processing text to words"""
    def __init__(self):
        from nltk import PorterStemmer
        self.porter = PorterStemmer()

    def extractWords(self, text, stopWordsSet):
        """ Extracts all non-stopwords words from text and stem them.

        Parameters
        ----------
        text         : string
                       First argument
        stopWordsSet : set of string values
                       Second argument

        Returns
        -------
        wordList : list of string values
                   Returns all stemmed non-stopwords words from text.
        wordSet  : set of string values
                   Returns all different stemmed non-stopwords words from text.
        """
        textLinesList = text.split("\n")
        
        wordList = []
        for line in textLinesList:
            # Split sentence to words, make them lower cased,
            # strip them, stem with PorterStemer and put in list.
            wordList.extend(
                map(lambda x:
                    self.porter.stem(x.strip(" \t\n.,!?%<>{}[](-)\'\"*;")\
                                .lower()), 
                    line.split(" ")))
        wordSet = set(wordList)
        wordSet.difference_update(stopWordsSet)
        return wordList, wordSet
            
        
        
def getStopWords(path):
    """Reads all words from file in path

    Parameters
    ----------
    path : string
           A path to a file.
           NOTE: a file in path should be structured as "word1\\nword2\\n..."

    Returns
    -------
    Set of all words from file in path + " ". Words are strings.
    """
    stopWords = set([word.strip() for word in open(path,"r").readlines()])
    stopWords.update(set([""]))
    return stopWords

def allQueryNamesFromFolder(queriesFolderPath):
    """ For given path returns sorted list of all query names in folder.

    Parameters
    ----------
    queriesFolderPath : string
                        First argument. Path to folder.
                        Example: C:/Queries/
                        NOTE: there is a slash in the end!
    Returns
    -------
    List of query file names in sorted-by-name order

    NOTE:  There must be only query files in the folder
    NOTE2: List contains names of the files, not paths
    """
    import os
    queryList = os.listdir(queriesFolderPath)
    queryList = [ v for v in queryList if v.startswith('query') ] # filter all except query files
    queryList.sort(key = lambda queryName: int(queryName.split("y")[1].strip(".txt")) )
    return queryList

def allArticleNamesFromFolder(articlesFolderPath):
    """ For given path returns sorted list of all article names in folder

    Parameters
    ----------
    articlesFolderPath : string
                        First argument. Path to folder.
                        Example: C:/Articles/
                        NOTE: there is a slash in the end!
    Returns
    -------
    List of article file names in sorted-by-name order

    NOTE: There must be only article files in the folder
    """
    import os
    fileList = os.listdir(articlesFolderPath)
    # "article-134.xml" --> [article, 134.xml]
    # "134.xml".strip(".xml") --> "134"
    fileList.sort(key = lambda fileName: \
                  int(fileName.split("-")[1].strip(".xml")) )
    return fileList

